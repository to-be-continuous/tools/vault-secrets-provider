/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"os"
	"testing"
)

func Test_getenv_when_var_unset(t *testing.T) {
	os.Unsetenv("TESTVAR")
	// os.Setenv("TESTVAR", "test")
	val := EnvStr("TESTVAR").Or("default")
	if val != "default" {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", "default", val)
	}
}

func Test_getenv_when_var_empty(t *testing.T) {
	os.Setenv("TESTVAR", "")
	val := EnvStr("TESTVAR").Or("default")
	if val != "default" {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", "default", val)
	}
}

func Test_getenv_when_var_gitlab_unset(t *testing.T) {
	os.Setenv("TESTVAR", "$TESTVAR")
	val := EnvStr("TESTVAR").Or("default")
	if val != "default" {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", "default", val)
	}
}

func Test_getenv_when_var_set(t *testing.T) {
	os.Setenv("TESTVAR", "testval")
	val := EnvStr("TESTVAR").Or("default")
	if val != "testval" {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", "testval", val)
	}
}

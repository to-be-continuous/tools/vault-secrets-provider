# included templates
include:
  # $TBC_NAMESPACE is a group variable; can be globally overridden
  # Docker template
  - component: "$CI_SERVER_FQDN/$TBC_NAMESPACE/docker/gitlab-ci-docker@6"
    inputs:
      build-args: "--build-arg CI_PROJECT_URL --build-arg DEFAULT_VAULT_URL"
      prod-publish-strategy: "auto"
      release-extra-tags: "latest \\g<major>.\\g<minor>\\g<build> \\g<major>\\g<build>"
  # Go template
  - component: "$CI_SERVER_FQDN/$TBC_NAMESPACE/golang/gitlab-ci-golang@4"
    inputs:
      build-flags: -tags netgo
      # force application build
      build-mode: application
      sbom-opts: "-main cmd/vault_service"
  # semantic-release template
  - component: "$CI_SERVER_FQDN/$TBC_NAMESPACE/semantic-release/gitlab-ci-semrel@3"
    inputs:
      auto-release-enabled: true
  # Gitleaks template
  - component: "$CI_SERVER_FQDN/$TBC_NAMESPACE/gitleaks/gitlab-ci-gitleaks@2"

# this job tests the Docker image on a remote Vault server (configurable)
test-on-remote:
  image: "registry.hub.docker.com/curlimages/curl"
  services:
    - name: "$DOCKER_SNAPSHOT_IMAGE"
      alias: "vault-secrets-provider"
  variables:
    # variables have to be explicitly declared in the YAML to be exported to the service
    VAULT_BASE_URL: "$TEST_VAULT_BASE_URL"
    VAULT_ROLE_ID: "$TEST_VAULT_ROLE_ID"
    VAULT_TOKEN: "$TEST_VAULT_TOKEN"
    VAULT_SECRET_ID: "$TEST_VAULT_SECRET_ID"
    VAULT_AUTH_METHOD: "auto"
  stage: package-test
  script:
    - curl -s -S -f "http://vault-secrets-provider/health"
    - |
      if [[ "$TEST_SECRET_PATH" ]]; then
        my_secret=$(curl -s -S -f "http://vault-secrets-provider/api/secrets/$TEST_SECRET_PATH?field=$TEST_SECRET_FIELD")
        echo "secret retrieved - $my_secret"
      else
        echo "no secret to test - skip"
      fi
    # TODO: test a client error
  rules:
    - if: '$TEST_VAULT_BASE_URL == null || $TEST_VAULT_BASE_URL == ""'
      when: never
    - !reference [.test-policy, rules]

# this job tests the Docker image on a local Vault server using the Vault server image
# See: https://hub.docker.com/_/vault
test-on-local:
  image: "registry.hub.docker.com/badouralix/curl-jq"
  services:
    - name: "$DOCKER_SNAPSHOT_IMAGE"
      alias: "vault-secrets-provider"
    - name: "hashicorp/vault"
      alias: "vault-server"
  variables:
    # variables have to be explicitly declared in the YAML to be exported to the service
    # config for Vault dev mode - see: https://www.vaultproject.io/docs/commands/server#dev-options
    VAULT_DEV_ROOT_TOKEN_ID: "dev-root-token"
    # defines the Vault server base URL for Vault Secrets Provider
    VAULT_BASE_URL: "http://vault-server:8200/v1" # config for
    # defines the Vault (root) Token for Vault Secrets Provider
    VAULT_TOKEN: "dev-root-token"
    # This allows the main container to connect to the services containers
    FF_NETWORK_PER_BUILD: "1"
  stage: package-test
  script:
    # wait for a while that all services are UP and running
    - sleep 5
    # check Vault Secrets Provider is UP and healthy
    - curl -sSf "http://vault-secrets-provider/health"
    # check Vault is UP and healthy
    - curl -sSf "$VAULT_BASE_URL/sys/health"
    # create a secret in Vault
    - |
      curl --silent --header "X-Vault-Token: ${VAULT_DEV_ROOT_TOKEN_ID}" --request PUT --data '{"data": {"foo": "bar", "zip": "zap", "dede.1": "toto"}}' "${VAULT_BASE_URL}/secret/data/my-secret"
    # test: get existing secret shall succeed
    - |
      resp_status=$(curl -s -o "resp.txt" -w "%{http_code}" "http://vault-secrets-provider/api/secrets/my-secret?field=foo")
      if [[ "$resp_status" != "200" ]]; then
        echo "FAILED get existing secret ($resp_status)"
        cat resp.txt
        exit 1
      fi
    # test: get existing secret with escaping character shall succeed
    - |
      resp_status=$(curl -s -o "resp.txt" -w "%{http_code}" "http://vault-secrets-provider/api/secrets/my-secret?field=dede\.1")
      if [[ "$resp_status" != "200" ]]; then
        echo "FAILED get existing secret ($resp_status)"
        cat resp.txt
        exit 1
      fi
    # test: get secret with non existing path shall fail with code 404
    - |
      resp_status=$(curl -s -o "resp.txt" -w "%{http_code}" "http://vault-secrets-provider/api/secrets/no/such/path?field=foo")
      if [[ "$resp_status" != "404" ]]; then
        echo "FAILED get secret with non existing path ($resp_status)"
        cat resp.txt
        exit 1
      fi
    # test: get secret with non existing field shall fail with code 400
    - |
      resp_status=$(curl -s -o "resp.txt" -w "%{http_code}" "http://vault-secrets-provider/api/secrets/my-secret?field=baz")
      if [[ "$resp_status" != "400" ]]; then
        echo "FAILED get secret with non existing field ($resp_status)"
        cat resp.txt
        exit 1
      fi
    # test: create secret shall succeed
    - |
      resp_status=$(curl -s -o "resp.txt" -w "%{http_code}" -X PUT --data '{"key":"value"}' "http://vault-secrets-provider/api/secrets/new-secret")
      if [[ "$resp_status" != "200" ]]; then
        echo "FAILED create secret ($resp_status)"
        cat resp.txt
        exit 1
      fi
    # test: get created secret shall succeed
    - |
      resp_status=$(curl -s -o "resp.txt" -w "%{http_code}" "http://vault-secrets-provider/api/secrets/new-secret?field=key")
      if [[ "$resp_status" != "200" ]]; then
        echo "FAILED get created secret ($resp_status)"
        cat resp.txt
        exit 1
      fi
    # test: delete secret shall succeed
    - |
      resp_status=$(curl -s -o "resp.txt" -w "%{http_code}" -X DELETE "http://vault-secrets-provider/api/secrets/new-secret")
      if [[ "$resp_status" != "204" ]]; then
        echo "FAILED delete secret ($resp_status)"
        cat resp.txt
        exit 1
      fi
    # # test: get deleted secret shall fail with code 404
    # - |
    #   resp_status=$(curl -s -o "resp.txt" -w "%{http_code}" "http://vault-secrets-provider/api/secrets/new-secret?field=key")
    #   if [[ "$resp_status" != "404" ]]; then
    #     echo "FAILED get secret with non existing path ($resp_status)"
    #     cat resp.txt
    #     exit 1
    #   fi
  rules:
    - !reference [.test-policy, rules]

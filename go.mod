module vault-secrets-provider

go 1.22.7

toolchain go1.24.1

require google.golang.org/grpc v1.71.0

require golang.org/x/tools/gopls v0.6.9 // indirect
